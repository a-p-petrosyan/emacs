;;; init --- Initialisation file for Emacs

;;; Commentary:
;; Emacs Startup File --- initialisation for Emacs

;;; Code:

(setq read-process-output-max (* 1024 1024))
(setq gc-cons-percentage 0.2)

(defvar first-run nil)
(defun notify-send (message)
  "Send a desktop notification with the given MESSAGE."
  (interactive (list (read-string "Message: ")))
  (let ((cmd (concat "notify-send "
					 "'" message "'"
					 " -i emacs"
					 " -a Emacs"
					 " -t 5000")))
	(when first-run (shell-command cmd))))

(require 'package)
;; Any add to list for package-archives (to add marmalade or melpa) goes here
(add-to-list 'package-archives
			 '("MELPA" .
			   "http://melpa.org/packages/"))
(package-initialize)

(notify-send "Installing essential packages")

(use-package delight
  :ensure t)

(use-package autorevert
  :delight auto-revert-mode)

(add-to-list 'load-path (concat user-emacs-directory "elisp/"))

(require 'personal)
(require 'packages)
(require 'major-modes)
(require 'appearance)
(require 'snippets)
(require 'keybinds)

(provide 'init)
;;; init.el ends here


(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auth-sources '(default))
 '(blink-cursor-blinks 3)
 '(blink-cursor-interval 0.3)
 '(c-default-style
   '((c++-mode . "bsd") (java-mode . "java") (awk-mode . "awk")
	 (other . "gnu")))
 '(cua-normal-cursor-color 'bar)
 '(cursor-type 'bar)
 '(custom-enabled-themes '(app-monochrome-themes-dark-theme))
 '(custom-safe-themes
   '("1f57e271671474bcc878681e98424ff6aa56999f26a1b177fc1e14e97a0d232d"
	 "632ca9c171bbbe15b310a2cfd2a2e5005ce26530360dca77e2c600e008c34806"
	 "2aa1d824591abcb14d7e6ed285043e82eaeae5e784efd34bda22b27301bf629b"
	 "c7fb1c0e2ebdfb73f0ccee8e536ba1978aa02dac17e2a18fd79e8ed6cbb3dcbf"
	 default))
 '(global-corfu-mode t)
 '(marginalia-align 'left)
 '(marginalia-mode t)
 '(markdown-enable-highlighting-syntax t)
 '(markdown-unordered-list-item-prefix "  - ")
 '(olivetti-style 'fancy)
 '(package-selected-packages
   '(aas abridge-diff aggressive-indent all-the-icons-completion
		 all-the-icons-dired apheleia app-monochrome-themes async
		 auctex buffer-name-relative bug-hunter cape cargo-mode
		 cargo-transient cdlatex color-identifiers-mode
		 consult-company consult-eglot context-transient corfu crdt
		 dap-mode delight diff-hl difftastic diminish dirvish
		 disposable-key docker docker-compose-mode dockerfile-mode
		 dotenv-mode drag-stuff dtrt-indent dumb-jump eglot-x
		 electric-operator elisp-autofmt ellama elmacro elpy elpygen
		 embark-consult emmet-mode eshell-git-prompt
		 eshell-syntax-highlighting expreg fancy-compilation
		 flymake-actionlint flymake-hadolint flymake-languagetool
		 flymake-proselint forge gcmh git-grep helpful
		 highlight-defined highlight-indent-guides jinx json-mode
		 key-quiz kind-icon kotlin-mode langtool languagetool logview
		 lsp-mode lsp-treemacs lsp-ui lsp-ui-mode magit-file-icons
		 marginalia mmm-mode multiple-cursors nix-mode olivetti
		 orderless org-elp org-modern package-lint paradox
		 pkgbuild-mode projection-dape projection-multi
		 projection-multi-embark rainbow-delimiters rainbow-mode
		 realgud rmsbolt rust-mode scopeline simple-httpd smartparens
		 solidity-mode string-inflection tabby-mode tide
		 transient-posframe treemacs undo-tree vertico
		 vertico-posframe vundo web-mode which-key zoom))
 '(package-vc-selected-packages
   '((highlight-defined :url
						"https://github.com/Fanael/highlight-defined"
						:branch "master")
	 (disposable-key :url
					 "https://github.com/Greybeard-Entertainment/disposable-key.git"
					 :branch "barba")
	 (eglot-x :url "https://github.com/nemethf/eglot-x" :branch "main")))
 '(safe-local-variable-values
   '((jinx-local-words . "Ok Recurse el fn frac impl todo")
	 (jinx-local-words . "Fira Plex gmail")
	 (jinx-local-words . "Fira Plex el gmail")
	 (jinx-local-words
	  . "DEs Difftastic Greybeard Petrosyan barba devs dwim emph")))
 '(show-trailing-whitespace t)
 '(tabby-api-url "\"http://localhost:8080\"")
 '(which-key-allow-imprecise-window-fit t)
 '(which-key-compute-remaps t)
 '(which-key-ellipsis "...")
 '(which-key-idle-delay 2.0)
 '(which-key-mode t)
 '(zoom-minibuffer-preserve-layout nil)
 '(zoom-mode t nil (zoom))
 '(zoom-size '(100 . 24)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(dired-directory ((t (:weight bold))))
 '(dirvish-media-info-property-key ((t nil)))
 '(lsp-ui-doc-background ((t (:background "grey10")))))

(put 'secrets-mode 'disabled nil)
