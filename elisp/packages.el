;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;;; General purpose packages.

;;; Code:

;; To silence the warning about `ispell-lookup-words` `paru -S extra/words`.
(use-package corfu
  :ensure t
  ;; TAB-and-Go customizations
  :custom
  (corfu-cycle t)           ;; Enable cycling for `corfu-next/previous'
  (corfu-preselect 'prompt) ;; Always preselect the prompt
  (corfu-auto t)
  ;; Use TAB for cycling, default is `corfu-complete'.
  :bind
  (:map corfu-map
        ("TAB" . corfu-next)
        ([tab] . corfu-next)
        ("S-TAB" . corfu-previous)
        ([backtab] . corfu-previous))
  :init (global-corfu-mode))

(use-package cape
  :ensure t
  :init
  (add-to-list 'completion-at-point-functions #'cape-file))

(use-package kind-icon
  :ensure t
  :after corfu
  :custom
  (kind-icon-blend-background t)
  (kind-icon-default-face 'corfu-default) ; only needed with blend-background
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(use-package docker-compose-mode
  :disabled
  :ensure t)

(use-package transient-posframe
  :ensure t
  :config
  (transient-posframe-mode +1))

(use-package docker
  :ensure t
  :disabled
  :bind ("C-c d" . docker))

(use-package smerge-mode
  :defer t
  :config
  (transient-define-prefix smerge-transient ()
	:transient-suffix 'transient--do-stay
	:transient-non-suffix 'transient-do-warn
	["Navigation"
	 ("n" "Next conflict" smerge-next)
	 ("p" "Previous conflict" smerge-previous)]
	["Conflict resolution"
	 ("<up>" "Keep upper (TODO, figure out if it's ours or theirs)" smerge-keep-upper)
	 ("<down>" "Keep lower (TODO, figure out if it's ours or theirs)" smerge-keep-lower)
	 ("C-c" "Keep the one where the point is" smerge-keep-current)])
  :custom
  (smerge-command-prefix (kbd "M-m"))
  :bind
  (:map smerge-mode-map
		("M-m M-m" . smerge-transient)))

(use-package vundo
  :ensure t)

(use-package avy
  :ensure t
  :bind
  ("C-l" . avy-goto-word-0)
  ("M-l" . avy-goto-char)
  ("M-g c" . avy-goto-char)
  ("M-g M-g" . avy-goto-line)
  ("M-g m" . avy-pop-mark))

(use-package dtrt-indent
  :ensure t
  :custom
  (dtrt-indent-global-mode t))

(use-package apheleia
  :ensure t
  :delight
  :hook
  (rust-mode . apheleia-mode)
  (emacs-lisp-mode . apheleia-mode)
  (c-mode . apheleia-mode)
  (cc-mode . apheleia-mode))

(use-package disposable-key
  :ensure t
  :vc
  (:url "https://github.com/Greybeard-Entertainment/disposable-key.git"
		:branch "barba")
  :bind
  ("<f6>" . disposable-key-bind)
  ("<f7>" . disposable-key-bind)
  ("<f8>" . disposable-key-bind)
  ("<f9>" . disposable-key-bind)
  ;; f10 -- menu most DEs
  ;; f11 -- full-screen. Why would you want that IDK, but it doesn't hurt anyone
  ("<f12>" . disposable-key-bind)
  ("C-x M-r" . disposable-key-force-rebind))

(use-package dirvish
  :ensure t
  :config
  (dirvish-override-dired-mode))


(use-package dape
  :ensure t)

(use-package projection-dape
  :ensure t)

(use-package dumb-jump
  :ensure t
  :config
  (add-hook 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package rmsbolt
  :ensure t)

(use-package projection
  :ensure t
  ;; Enable the `projection-hook' feature.
  :hook (after-init . global-projection-hook-mode)

  ;; Require projections immediately after project.el.
  :config
  (with-eval-after-load 'project
	(require 'projection))

  :config
  ;; Uncomment if you want to disable prompts for compile commands customized in .dir-locals.el
  ;; (put 'projection-commands-configure-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-build-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-test-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-run-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-package-project 'safe-local-variable #'stringp)
  ;; (put 'projection-commands-install-project 'safe-local-variable #'stringp)

  ;; Access pre-configured projection commands from a keybinding of your choice.
  ;; Run `M-x describe-keymap projection-map` for a list of available commands.
  :bind-keymap
  ("C-x P" . projection-map))

(use-package projection-multi
  :ensure t
  ;; Allow interactively selecting available compilation targets from the current
  ;; project type.
  :bind
  ("<f5>" . projection-multi-compile))

(use-package projection-multi-embark
  :ensure t
  :after embark
  :after projection-multi
  :demand t
  ;; Add the projection set-command bindings to `compile-multi-embark-command-map'.
  :config (projection-multi-embark-setup-command-map))

(use-package jinx
  :ensure t
  :config
  (jinx--load-module)
  (make-thread #'(lambda()(jinx--mod-dict "any-lang") "my/jinx-init"))
  :hook (emacs-startup . global-jinx-mode)
  :bind (("M-$" . jinx-correct)
		 ("C-M-$" . jinx-languages)))

(use-package crdt
  :ensure t
  :config
  (setq crdt-use-tuntox 'confirm))

(use-package color-identifiers-mode
  :ensure t
  :delight
  :hook (emacs-lisp-mode . color-identifiers-mode)
  :hook (python-mode . color-identifiers-mode)
  :hook (rust-ts-mode . color-identifiers-mode))

(use-package helpful
  :ensure t
  :after transient
  :bind
  ("C-h" . helpful-help-transient)
  :config
  (transient-define-prefix helpful-help-transient ()
	[("f" "Function or command" helpful-command)
	 ("F" "Face" describe-face)
	 ("c" "Char" describe-char)
	 ("m" "Mode" describe-mode)
	 ("K" "Keymap" describe-keymap)
	 ("s" "Symbol, or emacs non-interactive function" helpful-symbol)
	 ("m" "Macro (for elisp development)" helpful-macro)
	 ("v" "Variable" helpful-variable)
	 ("k" "Function that is bound to this key sequence" helpful-key)]))

(use-package electric-operator
  :ensure t
  :disabled
  :hook (python-mode . electric-operator-mode))

(use-package treemacs
  :ensure t
  :bind
  ("s-t" . treemacs-select-window))

(use-package aggressive-indent
  :ensure t
  :disabled
  :delight
  :bind (("C-x <C-tab>" . aggressive-indent-mode))
  :hook ((emacs-lisp-mode rust-mode LaTeX-mode) . aggressive-indent-mode))

(use-package async
  :ensure t)

(use-package git-grep
  :ensure t
  :defer t
  :after projectile
  :commands (git-grep git-grep-repo)
  :bind ("C-c g" . consult-git-grep))

(use-package string-inflection
  :ensure t
  :config
  (transient-define-prefix string-inflection-transient ()
	:transient-suffix 'transient--do-stay
	:transient-non-suffix 'transient--do-exit
	[["String inflection"
	  ("c" "Capitalise" string-inflection-capital-underscore)
	  ("C" "PascalCase" string-inflection-camelcase)
	  ("C-c" "(lower)camelCase" string-inflection-lower-camelcase)
	  ("l" "downcase word" downcase-dwim)
	  ("U" "UPCASE word" upcase-dwim)
	  ("u" "UPCASE_UNDERSCORE" string-inflection-upcase)
	  ("]" "Cycle" string-inflection-cycle)
	  ("[" "Cycle all" string-inflection-all-cycle)]
	 ["Navigation"
	  ("f" "forward word" right-word)
	  ("b" "backward word" left-word)
	  ("F" "Forward S-Exp" forward-sexp)
	  ("B" "Backward S-Exp" backward-sexp)
	  ("<left>" "backward char" backward-char)
	  ("<right>" "forward char" forward-char)]])
  :bind
  ("M-c" . string-inflection-transient))

(use-package expreg
  :ensure t
  :bind ("M-w" . expreg-expand))

(use-package smartparens
  :ensure t
  :defer t
  :diminish smartparens-mode
  :bind
  ("M-p" . sp-transient)
  :custom
  (sp-autoinsert-pair nil "Don't like it")
  (sp-autoskip-closing-pair "Even worse than auto-insert")
  (sp-wrap-entire-symbol 'globally)
  :hook
  (prog-mode . smartparens-mode)
  :hook
  (org-mode . smartparens-mode)
  :config
  (transient-define-prefix sp-transient--left ()
	:transient-non-suffix 'transient--do-exit
	["Left paren"
	 ("[" "Backward slurp" sp-backward-slurp-sexp :transient t)
	 ("]" "Backward barf" sp-backward-barf-sexp :transient t)])

  (transient-define-prefix sp-transient--right ()
	:transient-non-suffix 'transient--do-exit
	["Right paren"
	 ("]" "Forward slurp" sp-forward-slurp-sexp :transient t)
	 ("[" "Forward barf" sp-forward-barf-sexp :transient t)])

  (transient-define-prefix sp-transient ()
	:transient-non-suffix 'transient--do-call
	["Move delimiter"
	 ("[" "Left paren" sp-transient--left)
	 ("]" "Right paren" sp-transient--right)]
	["Navigate"
	 ("<down>" "Next nested S-expression" sp-down-sexp :transient t)
	 ("<up>" "Previous nested S-expression" sp-up-sexp :transient t)
	 ("f" "Forward" forward-sexp :transient t)
	 ("b" "Backward" backward-sexp :transient t)]
	["General"
	 :class transient-row
	 ("r" "Rewrap" sp-rewrap-sexp)
	 ("j" "Join" sp-join-sexp)
	 ("k" "Kill" sp-kill-sexp)
	 ("e" "Emit" sp-emit-sexp)
	 ("s" "Split" sp-split-sexp)
	 ("u" "Unwrap" sp-unwrap-sexp)])


  (require 'smartparens-config)
  ;;; Haskell
  (add-to-list 'sp-no-reindent-after-kill-modes 'haskell-mode)
   ;;; org-mode
  (defun sp--org-skip-asterisk (ms mb me)
	(or (and (= (line-beginning-position) mb)
			 (eq 32 (char-after (1+ mb))))
		(and (= (1+ (line-beginning-position)) me)
			 (eq 32 (char-after me)))))

  (defun sp--org-inside-LaTeX (id action context)
	(org-inside-LaTeX-fragment-p))

  (sp-with-modes 'org-mode
	(sp-local-pair "*" "*"
				   :unless '(sp-point-after-word-p sp--org-inside-LaTeX sp-point-at-bol-p)
				   :skip-match 'sp--org-skip-asterisk)
	(sp-local-pair "/" "/"
				   :unless '(sp-point-after-word-p sp--org-inside-LaTeX))
	(sp-local-pair "~" "~"
				   :unless '(sp-point-after-word-p sp--org-inside-LaTeX))
	(sp-local-pair "=" "="
				   :unless '(sp-point-after-word-p sp--org-inside-LaTeX))
	(sp-local-pair "\\[" "\\]")
	(sp-local-pair "\\(" "\\)")
	(sp-local-pair "\emph{" "}")))



(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :custom
  (magit-section-visibility-indicator '("…" . t))
  (magit-revision-fill-summary-line 120)
  (magit-revision-show-gravatars t)
  (magit-revision-use-gravatar-kludge t))

(use-package forge
  :after magit
  :custom
  (auth-sources '(default) "We use the secret service API")
  :ensure t)

(use-package difftastic
  :ensure t
  :config
  (eval-after-load 'magit-diff
	'(transient-append-suffix 'magit-diff '(-1 -1)
	   [("D" "Difftastic diff (dwim)" difftastic-magit-diff)
		("S" "Difftastic show" difftastic-magit-show)])))

(use-package abridge-diff
  :ensure t
  :delight
  :hook
  (magit-mode . abridge-diff-mode))

(use-package multiple-cursors
  :ensure t
  :disabled
  :defer t
  :bind
  ("M-m" . multicursor-transient)
  :config
  (transient-define-prefix multicursor-transient ()
	["Multiple cursors"
	 ("m" "Mark more like this extended, using arrow keys" mc/mark-more-like-this-extended)
	 ("a" "Mark all like this" mc/mark-all-dwim)
	 ("t" "Mark SGML tag pair" mc/mark-sgml-tag-pair)]))

(use-package drag-stuff
  :ensure t
  :delight
  :hook
  (prog-mode . drag-stuff-mode)
  :bind ("C-t" . drag-stuff-transient)
  :config
  (transient-define-prefix drag-stuff-transient ()
	:transient-non-suffix 'transient--do-call
	:transient-suffix 'transient--do-stay
	["Move"
	 ("<up>" "Up" drag-stuff-up)
	 ("<down>" "Down" drag-stuff-down)
	 ("<left>" "Left" drag-stuff-left)
	 ("<right>" "Right" drag-stuff-right)]
	["Transpose"
	 ("s" "S-Expression" sp-transpose-sexp)
	 ("w" "Words" transpose-words)
	 ("p" "Paragraphs" transpose-paragraphs)
	 ("c" "Single character" transpose-chars)]))

(use-package ellama
  :ensure t
  :init
  (setopt ellama-language "English")
  (require 'llm-ollama)
  (setopt ellama-provider
		  (make-llm-ollama
		   :chat-model "codestral" :embedding-model "codestral")))

(use-package buffer-name-relative
  :ensure t
  :disabled
  :config
  (buffer-name-relative-mode))

(use-package key-quiz
  :ensure t)

(provide 'packages)
;;; packages.el ends here

;; Local Variables:
;; jinx-local-words: "DEs Difftastic Greybeard Petrosyan barba devs dwim emph"
;; End:
