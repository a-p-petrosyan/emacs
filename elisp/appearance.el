;;; package --- Summary  -*- lexical-binding: t; -*-
;;; Commentary:
;; None

;;; Code:
(notify-send "Tweaking appearance")

(use-package all-the-icons
  :ensure t
  :custom
  (all-the-icons-completion-mode t)
  (all-the-icons-dired-monochrome nil))

(use-package transient
  :ensure t
  :bind (:map outline-mode-map ("C-n" . outline-navigate))
  :config
  (transient-define-prefix outline-navigate ()
	:transient-suffix     'transient--do-stay
	:transient-non-suffix 'transient--do-warn
	[("p" "previous visible heading" outline-previous-visible-heading)
	 ("n" "next visible heading" outline-next-visible-heading)]))

(use-package compile
  :ensure t
  :bind ("<f5>" . compile))

;;  M-x all-the-icons-install-fonts

(use-package all-the-icons-dired
  :ensure t
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package fancy-compilation
  :ensure t
  :hook ((prog-mode . fancy-compilation-mode)))

(use-package olivetti
  :ensure t
  :delight
  :bind
  ("C-<f11>" . olivetti-mode)
  ("C-S-<f11>" . variable-pitch-mode))
(use-package scopeline
  :ensure t
  :delight
  :hook (rust-ts-mode . scopeline-mode))

(use-package org-modern
  :ensure t
  :config
  (defun org-toggle-emphasis ()
	"Toggle hiding/showing of org emphasize markers."
	(interactive)
	(if org-hide-emphasis-markers
		(set-variable 'org-hide-emphasis-markers nil)
      (set-variable 'org-hide-emphasis-markers t))
	(org-mode-restart))
  (defalias 'org-toggle-markup 'org-toggle-emphasis)

  (defun my:split-sentence()
	"Add newline after the end of the sentence"
	(interactive)
	(forward-sentence)
	(org-return nil nil t))
  :bind
  ("s-." . my:split-sentence)
  ("C-s-<f11>" . org-toggle-emphasis)
  :hook (org-mode . org-modern-mode))

(use-package highlight-indent-guides
  :ensure t
  :disabled 
  :delight
  :hook (prog-mode . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-method 'bitmap)
  (highlight-indent-guides-responsive 'stack)
  :custom-face
  (highlight-indent-guides-character-face ((t (:foreground "light gray"))))
  (highlight-indent-guides-stack-character-face ((t (:foreground "white"))))
  (highlight-indent-guides-top-character-face ((t (:foreground "white"))))
  :config
  (setq highlight-indent-guides-auto-enabled nil)
  (set-face-background 'highlight-indent-guides-odd-face "darkred")
  (set-face-background 'highlight-indent-guides-even-face "dimgray")
  (set-face-foreground 'highlight-indent-guides-character-face "dimgray"))

(use-package rainbow-delimiters
  :ensure t
  :hook
  ((prog-mode LaTeX-mode org-mode) . rainbow-delimiters-mode)
  :custom-face
  (rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face))))
  (rainbow-delimiters-depth-2-face ((t (:inherit rainbow-delimiters-base-face :weight bold))))
  (rainbow-delimiters-depth-3-face ((t (:inherit rainbow-delimiters-base-face)))))

(use-package consult
  :ensure t
  :bind
  ("<f2>" . switch-to-minibuffer-or-consult-mode-command)
  ("C-S-s" . consult-ripgrep))

(use-package consult-company
  :after consult
  :ensure t)

(use-package vertico
  :ensure t
  :custom
  (vertico-buffer-mode nil "It doesn't save space and looks weird")
  (vertico-count 8 "10 was default. It's taking up too much space, to be honest")
  (vertico-count-format '("%-6s " . "%s of %s") "")
  (vertico-cycle t)
  (vertico-indexed-mode nil "Numbers in the left fringe don't work like they should")
  (vertico-mode t)
  (vertico-mouse-mode t)
  (vertico-multiform-mode t))

(use-package vertico-posframe
  :ensure t)

(use-package marginalia
  :ensure t
  :bind
  (:map minibuffer-local-map
		("<f2>" . marginalia-cycle))
  :init
  (use-package all-the-icons-completion
	:ensure t
	:hook (marginalia-mode . all-the-icons-completion-marginalia-setup))
  (marginalia-mode)
  :custom
  (marginalia-align-offset 20))

(use-package embark
  :ensure t
  :bind
  ("<f1>" . embark-act))

(use-package embark-consult
  :ensure t)


(use-package orderless
  :ensure t
  :custom
  (completion-styles '(orderless basic))
  (completion-category-overrides '((file (styles basic partial-completion))))
  (orderless-matching-styles '(orderless-initialism orderless-literal)))

(use-package zoom
  :ensure t
  :delight
  :bind ("C-x C-1" . zoom-mode))

(use-package which-key
  :ensure t
  :delight
  :config (which-key-mode))

(use-package capf-autosuggest
  :ensure nil
  :after company
  :hook ((eshell-mode . capf-autosuggest-mode)))

(use-package diff-hl
  :ensure t
  :after magit
  :hook (prog-mode . diff-hl-mode)
  :config
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))

(use-package magit-file-icons
  :ensure t
  :after magit
  :init
  (magit-file-icons-mode 1)
  :custom
  ;; These are the default values:
  (magit-file-icons-enable-diff-file-section-icons t)
  (magit-file-icons-enable-untracked-icons t)
  (magit-file-icons-enable-diffstat-icons t))

(defun ansi-colourise ()
  "Apply ANSI colour escape sequences.  Useful for Iroha logs."
  (interactive)
  (when (fboundp 'ansi-color-apply-on-region)
    (ansi-color-apply-on-region (point-min) (point-max))))

(setq prettify-symbols-alist
	  '(("lambda" . ?λ)
        ("->" . ?→)
        ("->>" . ?↠)
        ("=>" . ?⇒)
        ("/=" . ?≠)
        ("!=" . ?≠)
        ("==" . ?≡)
        ("<=" . ?≤)
        (">=" . ?≥)))

(add-hook 'prog-mode-hook 'prettify-symbols-mode)
(add-hook 'org-mode-hook 'prettify-symbols-mode)

(use-package eshell-git-prompt
  :ensure t
  :after eshell-mode
  :config (eshell-git-prompt-use-theme 'multiline2))

(use-package rainbow-mode
  :ensure t)

(use-package app-monochrome-themes
  :ensure t)

(use-package eshell-syntax-highlighting
  :after eshell-mode
  :ensure t
  :config
  ;; Enable in all Eshell buffers.
  (eshell-syntax-highlighting-global-mode +1))

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; Emacs 30 and newer: Disable Ispell completion function. As an alternative,
;; try `cape-dict'.
(setq text-mode-ispell-word-completion nil)

(provide 'appearance)
;;; appearance.el ends here
