;;; package --- Summary

;;; Commentary:
;;; Major-mode specific bindings

;;; Code:
(notify-send "Installing language-specific packages\na.k.a. major modes")

(setq major-mode-remap-alist
	  '((yaml-mode . yaml-ts-mode)
		(bash-mode . bash-ts-mode)
		(js2-mode . js-ts-mode)
		(typescript-mode . typescript-ts-mode)
		(json-mode . json-ts-mode)
		(css-mode . css-ts-mode)
		(python-mode . python-ts-mode)
		(rust-mode . rust-ts-mode)))

(setq treesit-language-source-alist
	  '((bash "https://github.com/tree-sitter/tree-sitter-bash")
		(cmake "https://github.com/uyha/tree-sitter-cmake")
		(css "https://github.com/tree-sitter/tree-sitter-css")
		(elisp "https://github.com/Wilfred/tree-sitter-elisp")
		(go "https://github.com/tree-sitter/tree-sitter-go")
		(html "https://github.com/tree-sitter/tree-sitter-html")
		(javascript "https://github.com/tree-sitter/tree-sitter-javascript" "master" "src")
		(json "https://github.com/tree-sitter/tree-sitter-json")
		(make "https://github.com/alemuller/tree-sitter-make")
		(markdown "https://github.com/ikatyang/tree-sitter-markdown")
		(python "https://github.com/tree-sitter/tree-sitter-python")
		(toml "https://github.com/tree-sitter/tree-sitter-toml")
		(tsx "https://github.com/tree-sitter/tree-sitter-typescript" "master" "tsx/src")
		(typescript "https://github.com/tree-sitter/tree-sitter-typescript" "master" "typescript/src")
		(yaml "https://github.com/ikatyang/tree-sitter-yaml")))

(use-package org
  :ensure nil
  :functions org-fill-paragraph
  :bind
  (:map org-mode-map
		("<f6>" . org-footnote-new)
		("M-." . full-stop)
		("<C-[>" . org-emphasize))
  :custom
  (org-edit-src-auto-save-idle-delay 3)
  (org-hide-emphasis-markers t)
  (org-edit-src-turn-on-auto-save t)
  (org-export-backends '(ascii html icalendar latex man md odt org texinfo))
  (org-export-creator-string "Aleksandr Petrosyan")
  (org-export-in-background t)
  (org-num-face 'org-code)
  :config
  (defun my:full-stop ()
	"Produce a full stop, fill column and save file."
	(interactive)
	(insert ".")
	(if (eolp)
		(progn
		  (org-fill-paragraph)
		  (save-buffer)
		  (insert " "))
	  (progn
		(save-excursion (capitalize-word 1))
		(insert " ")
		(forward-char 1)))))

(use-package cc-mode
  :ensure t
  :bind
  (:map c-mode-base-map
		("C-d" . kill-dwim))
  :hook (c-mode . eglot))

(use-package org-elp
  :ensure t
  :config
  (setq org-elp-idle-time 0.5
		org-elp-split-fraction 0.25))

(use-package pkgbuild-mode
  :ensure t)

(use-package markdown-mode
  :ensure t)

(use-package eglot
  :ensure t
  :bind
  (:map eglot-mode-map
		("C-c r" . eglot-rename)
		("C-c f" . eglot-format-buffer)
		("C-<return>" . eglot-code-actions)
		("C-<down-mouse-1>" . xref-find-definitions-at-mouse)
		("C-S-<down-mouse-1>" . xref-find-definitions-other-window)
		("M-<down-mouse-1>" . xref-find-references)
		("M-." . xref-find-definitions)
		("C-x 4 ." . xref-find-definitions-other-window))
  :custom
  (eglot-autoshutdown t)
  (eglot-extend-to-xref t)
  (eglot-send-changes-idle-time 2.0))

(use-package consult-eglot
  :ensure t
  :after consult
  :bind
  ("C-M-s" . consult-eglot-symbols))

(use-package lsp-mode
  :ensure t
  :bind
  (:map lsp-mode-map
		("C-." . xref-find-definitions)
		("C-c C-r" . lsp-rename)
		("C-<return>" . lsp-execute-code-action)
		("C-c m" . lsp-rust-analyzer-open-cargo-toml)
		("C-c f" . lsp-format-buffer))
  :custom
  (lsp-inlay-hint-enable t)
  (lsp-rust-analyzer-closure-return-type-hints "with_block")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-completion-enable nil)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  :hook
  (lsp-mode . lsp-inlay-hints-mode)
  (lsp-mode . yas-minor-mode))

(use-package lsp-ui
  :after lsp
  :ensure t)

(use-package dap-mode
  :ensure t
  :config
  (dap-ui-mode)
  (dap-ui-controls-mode 1)

  (require 'dap-lldb)
  (require 'dap-gdb-lldb)
  (dap-gdb-lldb-setup)

  (dap-register-debug-template
   "Rust::GDB Run Configuration"
   (list :type "gdb"
		 :request "launch"
		 :name "GDB::Run"
		 :gdbpath "rust-gdb"
		 :target nil
		 :cwd "${workspaceFolder}"
		 :dap-compilation "cargo build"
		 :dap-compilation-dir "${workspaceFolder}"))

  (with-eval-after-load 'dap-mode
	(setq dap-default-terminal-kind "integrated") ;; Make sure that terminal programs open a term for I/O in an Emacs buffer
	(dap-auto-configure-mode +1)))

(use-package lsp-treemacs
  :ensure t
  :bind
  (:map lsp-mode-map
		("C-`" . lsp-treemacs-errors-list)))

(use-package web-mode
  :ensure t)

(use-package solidity-mode
  :ensure t)

(use-package highlight-defined
  :ensure t
  :disabled
  :vc (:url "https://github.com/Fanael/highlight-defined" :branch "master")
  :hook (emacs-lisp-mode . highlight-defined-mode))

(use-package elisp-autofmt
  :ensure t
  :commands (elisp-autofmt-mode elisp-autofmt-buffer)
  :hook (emacs-lisp-mode . elisp-autofmt-mode))

(use-package rust-ts-mode
  :ensure nil
  :bind
  (:map rust-ts-mode-map
		("M-<return>" . my:rust:doc)
		("C-c i i" . my:rust:impl)
		("C-c i f" . my:rust:fn)
		("C-c i t" . my:rust:test)
		("C-c i o" . my:rust:ok)
		("C-c p" . my:rust:pub))
  :hook (rust-ts-mode . aggressive-indent-mode)

  :hook (rust-ts-mode . eglot-ensure)
  ;; :hook (rust-ts-mode . lsp)
  :hook (rust-ts-mode . subword-mode)
  ;; :hook (rust-ts-mode . apheleia-mode)
  )

(use-package rust-mode
  :ensure t)

(use-package projectile
  :delight
  :ensure t
  :hook (prog-mode . projectile-mode))

(use-package tide
  :ensure t
  :after (typescript-mode company)
  :hook (typescript-mode . tide-setup)
  :hook (typescript-mode . tide-hl-identifier-mode)
  :hook (before-save . tide-format-before-save))

(use-package cargo-mode                 ;TODO sane bindings
  :ensure t
  :hook (rust-mode . cargo-minor-mode))

(use-package emmet-mode
  :ensure t
  :hook (sgml-mode . emmet-mode))

(use-package cargo-transient
  :ensure t
  :custom
  (cargo-transient-buffer-name-function 'project-prefixed-buffer-name))

(use-package logview
  :ensure t)

(use-package kotlin-mode
  :ensure t)

(use-package yaml-mode
  :ensure t
  :defer 10
  :hook (yaml-mode . whitespace-mode))

(use-package json-mode
  :ensure t
  :config (setq js-indent-level 2)
  :bind (:map json-mode-map ("M-q" . json-pretty-print-buffer)))

(use-package flymake-hadolint
  :ensure t
  :hook (dockerfile-mode . flymake-hadolint-setup))

(use-package dockerfile-mode
  :ensure t)

(use-package tex
  :ensure auctex
  :defer 10
  :functions texmathp TeX-add-symbols
  ;; :hook (tex-mode . (lambda () (setq ispell-parser 'tex)))
  :config
  (defvar reftex-label-alist)
  (defvar reftex-plug-into-AUCTeX)
  (defvar ispell-parser)
  (defvar tex-mode-map)
  (setq reftex-label-alist '((nil ?e nil "~\\eqref{%s}" nil nil)))
  (setq reftex-plug-into-AUCTeX t)

  (defun insert-math-env ()
	"Surround the selected region with \\( and \\) in LaTeX,
 or insert the pair \\(\\) if no region is active."
	(interactive)
	(if (region-active-p)
		(let ((start (region-beginning))
			  (end (region-end)))
		  (goto-char end)
		  (insert "\\)")
		  (goto-char start)
		  (insert "\\("))
	  (insert "\\(\\)")))

  (defun undollar ()
	"Remove the obsolete LaTeX $ math mode delineation."
	(interactive)
	(if (texmathp)
		(progn (save-excursion (search-backward "$")
							   (delete-char 1)
							   (insert "\\("))
			   (save-excursion(search-forward "$")
							  (backward-delete-char 1)
							  (insert "\\)")))
	  (progn (search-forward "$")
			 (backward-delete-char 1)
			 (insert "\\(")
			 (search-forward "$")
			 (backward-delete-char 1)
			 (insert "\\)"))))

  ;; (setcar (cdr (assoc 'output-pdf TeX-view-program-selection)) "Okular")
  :hook (LaTeX-mode
		 . (lambda ()
			 (progn
			   (TeX-add-symbols '("eqref" TeX-arg-ref (ignore)))
			   (setq TeX-electric-math t)
			   (turn-on-reftex)))
		 )
  :bind
  (:map tex-mode-map
		("s-u" . undollar)
		("C-4" . insert-math-env))
  :custom
  (TeX-source-correlate-mode t)
  (TeX-source-correlate-start-server t))

(use-package cdlatex
  :ensure t
  :bind (:map cdlatex-mode-map
			  ("(" . LaTeX-insert-left-brace)
			  ("[" . LaTeX-insert-left-brace)
			  ("{" . LaTeX-insert-left-brace)
			  ("½" . my:LaTeX-insert-frac)
			  ("⅓" . my:LaTeX-insert-frac)
			  ("¼" . my:LaTeX-insert-frac)
			  ("⅕" . my:LaTeX-insert-frac)
			  ("⅖" . my:LaTeX-insert-frac)
			  ("⅗" . my:LaTeX-insert-frac)
			  ("⅘" . my:LaTeX-insert-frac)
			  ("⅙" . my:LaTeX-insert-frac)
			  ("⅐" . my:LaTeX-insert-frac)
			  ("⅛" . my:LaTeX-insert-frac)
			  ("⅔" . my:LaTeX-insert-frac)
			  ("$" . insert-math-env)
			  ;; ("s-]" . org-ref-helm-insert-ref-link)
			  ;; ("s-[" . org-ref-helm-insert-label-link)
			  )
  :custom
  (cdlatex-paired-parens "")
  (cdlatex-use-dollar-to-ensure-math nil))

(use-package dotenv-mode
  :ensure t)

(use-package flymake-actionlint
  :ensure t
  :hook
  (yaml-mode . flymake-actionlint-action-load-when-actions-file))

(use-package flymake-proselint
  :ensure t
  :hook
  (markdown-mode . (lambda () (progn (flymake-mode) (flymake-proselint-setup))))
  (org-mode . (lambda () (progn (flymake-mode) (flymake-proselint-setup)))))

(use-package languagetool
  :ensure t
  :defer t
  :commands (languagetool-check
			 languagetool-clear-suggestions
			 languagetool-correct-at-point
			 languagetool-correct-buffer
			 languagetool-set-language
			 languagetool-server-mode
			 languagetool-server-start
			 languagetool-server-stop)
  :config
  (setq languagetool-java-arguments '("-Dfile.encoding=UTF-8"
									  "-cp" "/usr/share/languagetool:/usr/share/java/languagetool/*")
		languagetool-console-command "org.languagetool.commandline.Main"
		languagetool-server-command "org.languagetool.server.HTTPServer"))


(use-package elpy
  :ensure t
  :init
  (elpy-enable)
  :hook ((python-mode . python-ts-mode))
  :config
  :bind (:map elpy-mode-map ("M-q" . elpy-format-code)))

(use-package tabby-mode
  :custom (tabby-api-url "http://localhost:8080")
  :ensure t)

(use-package elpygen
  :after elpy
  :ensure t)

(use-package package-lint
  :ensure t)

(use-package nix-mode
  :ensure t)

(provide 'major-modes)
;;; major-modes.el ends here

;; Local Variables:
;; jinx-local-words: "clippy eglot workspaceFolder"
;; End:
