;;; keybindings --- my personal key binds.

;;; Commentary:

;;; Code:

;; Less insane defaults:
(require 'personal)
(require 'packages)

(global-set-key (kbd "<redo>") 'undo)
(global-unset-key (kbd "s-k"))
(global-unset-key (kbd "C-r"))

;; Bringing emacs into the 21-st century
(define-key input-decode-map [?\C-i] (kbd "<C-i>"))
(define-key input-decode-map [?\C-m] (kbd "<C-m>"))
(define-key input-decode-map [?\C-\[] (kbd "<C-[>"))

(define-key emacs-lisp-mode-map (kbd "M-RET") 'insert-use-package)

;; Window
(global-set-key (kbd "C-c w") 'whitespace-cleanup)
(global-set-key (kbd "C-a") 'nav-back)
(global-set-key (kbd "<home>") 'nav-back)
(global-set-key (kbd "C-o") 'other-window)
(global-set-key (kbd "C-e") 'nav-forward)
(global-set-key (kbd "C-w") 'select-and-yank-dwim)
(global-set-key (kbd "<end>") 'nav-forward)
(global-set-key (kbd "<delete>") 'delete-forward-char-dwim)
(global-set-key (kbd "<backspace>") 'delete-backward-char-dwim)
(global-set-key (kbd "C-d") 'kill-dwim)
(global-set-key (kbd "C-S-r") 'cua-rectangle-mark-mode)
(global-set-key (kbd "s-r") 'revert-buffer)
(global-set-key (kbd "<C-[>") 'flymake-show-project-diagnostics)


(provide 'keybinds)
;;; keybinds.el ends here
